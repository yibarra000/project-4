#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
#include <fstream>
#include <set>
#include <iterator>
#include <vector>
#include <algorithm>
using std::endl;
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long optionsS
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};

	int lineCount();

	// process options:I
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {

			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;

			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	//1 -c
	int dupeAmt = 1;
	string theLine;
	string previousLine;
	//2 -d
	std::vector<string> sVec;
	string currentString;
	string printString;
	//3 -u
	bool dupeFound = false;
	int wait = 0;
	/* TODO: write me... */

	cout << "Enter Lines: " << endl;

	while (getline(cin, theLine)){

	bool wPrint = true; //will print

	// -d
	if (dupsonly) {
		//add the line into the vector and then compare each new line with the old ones.
			if (previousLine == theLine){

					dupeAmt++;
					previousLine = theLine;
					wPrint = false;
					if (dupeAmt == 2) {
						printString = theLine;
					}
			}else if (previousLine != theLine){
					previousLine = theLine;
					wPrint = true;
			}
		}

	// -u
	if (uniqonly && dupsonly == false){

		dupeFound = false;

		if (previousLine == theLine){
				previousLine = theLine;
				wait++;
				dupeFound = true;
			  if (previousLine != theLine){
				dupeFound = false;
				}
		}
		if (previousLine != theLine){

			if (showcount && dupeAmt == 1 && wait > 0 && dupeFound == false){
				cout << "\t" << dupeAmt << " " << previousLine << endl;
			}else if (dupeAmt == 1 && wait > 0 && dupeFound == false){
				cout << "\t" << previousLine << endl;
			}
			previousLine = theLine;
			wait++;
		}
	}

	// -c
	if (dupsonly && uniqonly){
		wPrint = false;
	}
	//cout << dupeAmt;
	if (showcount && wPrint && dupeAmt > 1){
			cout << "\t" << dupeAmt << " " << printString << endl;
			dupeAmt = 1;
	}else if (wPrint && dupeAmt > 1){
			cout << "\t" << printString << endl;
			dupeAmt = 1;
	}
}
	return 0;
}