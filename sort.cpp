#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <vector>
using std::string;
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
#include <fstream>
#include <bits/stdc++.h>
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */

void ignoreCase(vector<string> A)
{
	set <string,igncaseComp> S;
	for (int i=0; i<A.size(); i++)
	{
		while( cin>> A[i])
			{
				S.insert(A[i]);
			}
		}
			for (set<string, igncaseComp>::iterator i=S.begin(); i!=S.end(); i++)
						{
							cout << *i <<endl;
						}
		}


	void Unique(vector<string> B)
		{
			set <string> Unique;
			for (int i=0; i<B.size();i++)
			{
				cin>>B[i];
				Unique.insert(B[i]);
				}
					for (set<string>::iterator i=Unique.begin(); i!=Unique.end(); i++)
						{
							cout << *i <<endl;
						}
				}

void reverse(vector<string> C)
				{
					set <string> reverse;
					for (int i=0; i<C.size(); i++)
					{
						while (cin>>C[i])
						{
							reverse.insert(C[i]);
						}
					}

							for (set<string>::reverse_iterator i=reverse.rbegin(); i!=reverse.rend(); i++)
								{
									cout << *i <<endl;
								}
							}



int main(int argc, char *argv[]) {


	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	vector<string> E;
	string str;
	while (cin >> str)
	{
		ignoreCase(E);
		Unique(E);
		reverse(E);
	}
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
			reverse(E);
			break;

			case 'f':
				ignorecase = 1;
			ignoreCase(E);
			break;

			case 'u':
				unique = 1;
			Unique(E);
			break;

			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}



	return 0;
}