#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::swap;
using std::min;

int random(int s, int e);
vector<int> vecInd(vector<string> x);
vector<int> vecInd(vector<int> x);
vector<int> randVec(vector<int> z);
vector<string> perVec(vector<string> a, vector<int> b);
vector<int> perVec(vector<int> a, vector<int> b);
void printVec(vector<string> m);
void printVec(vector<int> m);
void printVecRange(vector<string> m,size_t x);


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	/* TODO: write me... */

	if(echo==1 && userange){
		cout<< "shuf: cannot combine -e  and -i options\n";}

	if(userange){
		vector<int> ints;
		for(int i = rlow; i < rhigh + 1; i++){
				ints.push_back(i);}
		vector<int> last = perVec(ints,randVec(vecInd(ints)));
		printVec(last);
				}

	if(echo==1){
		vector<string> first;
		while(optind < argc){
				first.push_back(argv[optind++]);}
		vector<string> last = perVec(first,randVec(vecInd(first)));
		printVec(last);}

	if(count > 0){
		vector<string> firstx;
		while(optind < argc){
				firstx.push_back(argv[optind++]);}
		vector<string> lastx = perVec(firstx,randVec(vecInd(firstx)));
		printVecRange(lastx,count);}

	if(echo != 1 && count == -1 && rlow == 0 && rhigh == 0){
		vector<string> first2;
		string x;
		while(getline(cin,x)){
			first2.push_back(x);}
		vector<string> last2 = perVec(first2,randVec(vecInd(first2)));
		printVec(last2);}

	return 0;
}

int random(int s, int e){
	return s+(rand()%e);}

vector<int> vecInd(vector<string> x){
		vector<int> y;
		for(int i = 0; i < x.size(); i++){
			y.push_back(i);}
		return y;}

vector<int> vecInd(vector<int> x){
		vector<int> y;
		for(int i = 0; i < x.size(); i++){
			y.push_back(i);}
		return y;}


vector<int> randVec(vector<int> z){
		int r, i;
		int sizeOfZ = z.size();
		vector<int> n;
		for(size_t j = 0; j < z.size(); j++){
			i = random(0,z.size());
			n.push_back(z[i]);
			z.erase(z.begin()+i);}
		return n; }

vector<string> perVec(vector<string> a, vector<int> b){
	vector<string> last;
	for(int i = 0; i < b.size(); i++){
		last.push_back(a[b[i]]);}
	return last;}

vector<int> perVec(vector<int> a, vector<int> b){
	vector<int> last;
	for(int i = 0; i < b.size(); i++){
		last.push_back(a[b[i]]);}
	return last;}

void printVec(vector<string> m){
		for(int i = 0; i < m.size(); i++){
			cout << m[i] << "\n";
		}}

void printVec(vector<int> m){
		for(int i = 0; i < m.size(); i++){
			cout << m[i] << "\n";
		}}

void printVecRange(vector<string> m,size_t x){
		for(size_t i = 0; i < x; i++){
			cout << m[i] << "\n";
		}}



